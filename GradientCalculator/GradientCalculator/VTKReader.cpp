#include "VTKReader.h"

#include <fstream>
#include <sstream>

using namespace GradientCalculator;

const char* pointsPrefix = "POINTS";
const char* pointDataPrefix = "POINT_DATA";
const char* valuesPrefix = "SCALARS";

VTKReader::VTKReader(Mesh& mesh) :
    Reader(ReaderType::VTKReader),
    _mesh(mesh)
{
}

VTKReader::~VTKReader()
{
}

bool VTKReader::ReadFile(const std::string& fileName)
{
    std::ifstream inputFile(fileName);

    if (!inputFile.is_open())
        return false;

    bool pointsRead = false;
    bool valuesRead = false;
    int numberOfPoints = 0;
    int numberOfValues = 0;

    std::string line;

    while (std::getline(inputFile, line))
    {
        // Read number of points
        if (numberOfPoints <= 0)
        {
            if (line.rfind(pointsPrefix, 0) == std::string::npos)
                continue;

            std::istringstream stringStream(line);
            std::string tmp;
            stringStream >> tmp >> numberOfPoints;
        }

        if (!pointsRead)
        {
            // Read points
            for (int i = 0; i < numberOfPoints; ++i)
            {
                std::getline(inputFile, line);
                std::istringstream stringStream(line);
                double x, y, z;
                stringStream >> x >> y >> z;

                _mesh.AddNode(x, y, z);
            }

            pointsRead = true;
        }

        if (!valuesRead)
        {
            // Read number of data
            if (line.rfind(pointDataPrefix, 0) == std::string::npos)
                continue;

            std::istringstream stringStream(line);
            std::string tmp;
            stringStream >> tmp >> numberOfValues;

            valuesRead = true;
        }


        if (line.rfind(valuesPrefix, 0) == std::string::npos)
            continue;

        std::istringstream stringStream(line);
        std::string tmp, valueName, valueType;
        double numberOfValuesPerPoint;
        
        stringStream >> tmp >> valueName >> valueType >> numberOfValuesPerPoint;

        std::getline(inputFile, line);
        std::getline(inputFile, line);

        stringStream.clear();
        stringStream.str(line);

        // Read values
        for (int i = 0; i < numberOfValues; ++i)
        {
            double v;
            stringStream >> v;

            _mesh.AddValue(v, valueName);
        }
    }

    return true;
}