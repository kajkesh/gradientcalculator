#include "CalculationsController.h"

#include "VTKReader.h"

using namespace GradientCalculator;

CalculationsController::CalculationsController()
{
}

CalculationsController::~CalculationsController()
{
}

void CalculationsController::RunCalculations(const std::string& inputFileName)
{
	VTKReader reader(_mesh);
	reader.ReadFile(inputFileName);
}