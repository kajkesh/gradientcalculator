#pragma once

#include <map>
#include <vector>

namespace GradientCalculator
{
	class Mesh
	{
	public:
		Mesh();
		~Mesh();

		void AddNode(double x, double y, double z);
		void AddValue(double value, std::string position);
		void AddNodes(std::vector<double> coordinates);
		void AddValues(std::vector<double> values, std::string position);
		int GetNodesCount();

	protected:
		std::vector<double> _nodes;
		std::map<std::string, std::vector<double>> _values;
	};
}