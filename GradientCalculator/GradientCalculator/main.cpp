#include "CalculationsController.h"

#include "dependencies/cxxopts.hpp"

using namespace GradientCalculator;

struct Arguments
{
	std::string inputPath;
	std::string outputPath;
};

int main(int argc, char* argv[])
{
	Arguments arguments;

	cxxopts::Options options("Gradient Calculator", "Calculate the gradient of a value for the point cloud");
	options.add_options()
		("i,input", "Path to the input .vtk file",
			cxxopts::value<std::string>(arguments.inputPath)->default_value("data/test_input.vtk"))
		("o,output", "Path to the output .vtk file",
			cxxopts::value<std::string>(arguments.outputPath)->default_value("data/test_output.vtk"))
		;
	options.parse(argc, argv);

	std::cout << options.help() << std::endl;

	CalculationsController controller;
	controller.RunCalculations(arguments.inputPath);

	system("pause");
	return 0;
}