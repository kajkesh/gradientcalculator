#pragma once

#include "Mesh.h"

#include <memory>

namespace GradientCalculator
{
	class CalculationsController
	{
	public:
		CalculationsController();
		~CalculationsController();

		void RunCalculations(const std::string& inputFileName);

	protected:
		Mesh _mesh;
	};
}