#pragma once

#include <string>

namespace GradientCalculator
{
	enum class ReaderType
	{
		VTKReader
	};

	class Reader
	{
	protected:
		Reader(ReaderType readerType);

	public:
		virtual ~Reader();
		virtual bool ReadFile(const std::string& fileName) = 0;

	protected:
		ReaderType _type;
	};
}