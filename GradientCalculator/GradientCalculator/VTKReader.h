#pragma once
#include "Reader.h"

#include "Mesh.h"

namespace GradientCalculator
{
	class VTKReader : public Reader
	{
	public:
		VTKReader(Mesh& mesh);
		~VTKReader();

		bool ReadFile(const std::string& fileName);

	private:
		Mesh& _mesh;
	};
}