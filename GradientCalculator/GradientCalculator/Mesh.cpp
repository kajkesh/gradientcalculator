#include "Mesh.h"

using namespace GradientCalculator;

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

void Mesh::AddNode(double x, double y, double z)
{
	_nodes.push_back(x);
	_nodes.push_back(y);
	_nodes.push_back(z);
}

void Mesh::AddValue(double value, std::string position)
{
	_values[position].push_back(value);
}

void Mesh::AddNodes(std::vector<double> coordinates)
{
	_nodes.insert(_nodes.end(), coordinates.begin(), coordinates.end());
}

void Mesh::AddValues(std::vector<double> values, std::string position)
{
	_values[position].insert(_values[position].end(), values.begin(), values.end());
}

int Mesh::GetNodesCount()
{
	return _nodes.size() / 3;
}